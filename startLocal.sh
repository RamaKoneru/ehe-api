#!/bin/bash

echo "Building the project.."
./gradlew clean build

java -jar -Xdebug -Xrunjdwp:transport=dt_socket,address=5050,server=y,suspend=n build/libs/authorization-service-0.0.1-SNAPSHOT.jar
