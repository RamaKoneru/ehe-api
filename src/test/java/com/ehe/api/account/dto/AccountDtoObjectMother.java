package com.ehe.api.account.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.ehe.api.account.domain.Gender;

public class AccountDtoObjectMother {

    public static final String DATE_OF_BIRTH = LocalDate.of(1970, 1, 31).format(DateTimeFormatter.ISO_DATE);

    public static AccountDto create() {
        AccountDto accountDTO = new AccountDto();
        accountDTO.setFirstName("Phil");
        accountDTO.setLastName("Filippelli");
        accountDTO.setGender(Gender.MALE.name());
        accountDTO.setDateOfBirth(DATE_OF_BIRTH);
        accountDTO.setEmail("Phil.Filippelli@ehe.health");
        return accountDTO;
    }

    public static AccountDto createForMismatching() {
        AccountDto accountDTO = new AccountDto();
        accountDTO.setFirstName("Stella");
        accountDTO.setLastName("Johnson");
        accountDTO.setGender(Gender.FEMALE.name());
        accountDTO.setDateOfBirth(DATE_OF_BIRTH);
        accountDTO.setEmail("Stella.Johnson@gmail.com");
        return accountDTO;
    }

    public static AccountDto createEmpty() {
        return new AccountDto();
    }
}