package com.ehe.api.account.exception;

import static com.ehe.api.account.exception.BadAccountRegistrationRequestException.Reason.USER_ALREADY_EXISTS;
import static com.ehe.api.account.exception.BadAccountRegistrationRequestException.Reason.USER_MATCH_NOT_FOUND;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import com.ehe.api.account.dto.ErrorDetails;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.http.ResponseEntity;

@RunWith(Parameterized.class)
public class AccountResponseEntityExceptionHandlerTest {

    private BadAccountRegistrationRequestException.Reason errorReason;
    private String expectedErrorMessage;

    private AccountResponseEntityExceptionHandler accountResponseEntityExceptionHandler = new AccountResponseEntityExceptionHandler();

    public AccountResponseEntityExceptionHandlerTest(BadAccountRegistrationRequestException.Reason errorReason, String expectedErrorMessage) {
        this.errorReason = errorReason;
        this.expectedErrorMessage = expectedErrorMessage;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataFixture() {
        return Arrays.asList(new Object[][]{
                {USER_MATCH_NOT_FOUND, USER_MATCH_NOT_FOUND.name()},
                {USER_ALREADY_EXISTS, USER_ALREADY_EXISTS.name()}
        });
    }

    @Test
    public void shouldReturnResponseForUserMismatchError() {
        BadAccountRegistrationRequestException exception = new BadAccountRegistrationRequestException(errorReason);

        ResponseEntity<Object> responseEntity = accountResponseEntityExceptionHandler.handleBadAccountRegistrationRequestException(exception);

        ErrorDetails errorDetails = (ErrorDetails) responseEntity.getBody();
        assertEquals(expectedErrorMessage, errorDetails.getError());
    }
}