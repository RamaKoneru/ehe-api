package com.ehe.api.account.exception;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.dto.AccountDtoObjectMother;
import com.ehe.api.account.dto.ErrorDetails;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@RunWith(Parameterized.class)
public class AccountResponseEntityExceptionHandler_ValidationErrorsTest {

    private String fieldName;
    private String fileError;

    private WebRequest webRequest = new ServletWebRequest(new MockHttpServletRequest(), new MockHttpServletResponse());
    private AccountDto accountDto = AccountDtoObjectMother.createEmpty();

    private AccountResponseEntityExceptionHandler accountResponseEntityExceptionHandler = new AccountResponseEntityExceptionHandler();

    public AccountResponseEntityExceptionHandler_ValidationErrorsTest(String fieldName, String fileError) {
        this.fieldName = fieldName;
        this.fileError = fileError;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataFixture() {
        return Arrays.asList(new Object[][]{
                {"firstName", "Invalid first name"},
                {"lastName", "Invalid last name"}
        });
    }

    @Test
    public void shouldReturnInvalidDataErrorMessageForFieldErrors() {
        MethodArgumentNotValidException methodArgumentNotValidException = setupFieldError(fieldName, fileError);

        ResponseEntity<Object> responseEntity = accountResponseEntityExceptionHandler.handleMethodArgumentNotValid(methodArgumentNotValidException, new HttpHeaders(), HttpStatus.BAD_REQUEST, webRequest);

        ErrorDetails errorDetails = (ErrorDetails) responseEntity.getBody();
        assertEquals("Invalid data.", errorDetails.getError());
    }

    private MethodArgumentNotValidException setupFieldError(String filedName, String fieldError) {
        String objectName = "accountDto";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(accountDto, objectName);
        bindingResult.addError(new FieldError(objectName, filedName, fieldError));
        return new MethodArgumentNotValidException(null, bindingResult);
    }
}