package com.ehe.api.account.controller;

import static org.junit.Assert.assertEquals;

import com.ehe.api.account.dto.AccountDtoObjectMother;
import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.dto.ErrorDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class AccountControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void successfulRegistrationShouldReturnResponseWith201StatusCode() throws Exception {
        RequestBuilder requestBuilder = createRequest(AccountDtoObjectMother.create());

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(201, mvcResult.getResponse().getStatus());
    }

    @Test
    public void emptyRequestShouldReturn400StatusCode() throws Exception {
        RequestBuilder requestBuilder = createRequest(AccountDtoObjectMother.createEmpty());

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());
    }

    @Test
    public void emptyRequestShouldReturnInvalidData() throws Exception {
        RequestBuilder requestBuilder = createRequest(AccountDtoObjectMother.createEmpty());

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        ErrorDetails errorDetails = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), ErrorDetails.class);
        assertEquals("Invalid data.", errorDetails.getError());
    }

    private RequestBuilder createRequest(AccountDto content) throws JsonProcessingException {
        return MockMvcRequestBuilders.request(HttpMethod.POST, "/member/account").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(content));
    }
}