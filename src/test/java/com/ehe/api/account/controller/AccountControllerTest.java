package com.ehe.api.account.controller;

import static org.junit.Assert.assertEquals;

import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.dto.AccountDtoObjectMother;
import com.ehe.api.account.service.AccountServiceObjectMother;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

public class AccountControllerTest {

    private AccountController accountController = new AccountController(AccountServiceObjectMother.create());

    @Test
    public void shouldReturnResponseEntityWith201StatusWhenUserMatchIsFound() {
        ResponseEntity<AccountDto> responseEntity = accountController.create(AccountDtoObjectMother.create());

        assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    public void shouldReturnAccountWithUserIdWhenUserMatchIsFound() {
        ResponseEntity<AccountDto> responseEntity = accountController.create(AccountDtoObjectMother.create());

        AccountDto accountRegistrationResponse = responseEntity.getBody();
        assertEquals(Long.valueOf("123"), accountRegistrationResponse.getUserId());
    }
}