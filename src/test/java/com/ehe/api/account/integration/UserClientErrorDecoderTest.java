package com.ehe.api.account.integration;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import com.ehe.api.account.exception.BadAccountRegistrationRequestException;

import feign.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class UserClientErrorDecoderTest {

    private static final String USER_ALREADY_REGISTERED_ERROR = "{\"error\": \"User already exists\"}";
    private static final String USER_NOT_FOUND_ERROR = "{\"error\": \"User not found\"}";
    private int errorCode;
    private String errorBody;
    private BadAccountRegistrationRequestException.Reason reasonForError;

    private UserClientErrorDecoder userClientErrorDecoder = new UserClientErrorDecoder();

    public UserClientErrorDecoderTest(int errorCode, String errorBody, BadAccountRegistrationRequestException.Reason reasonForError) {
        this.errorCode = errorCode;
        this.errorBody = errorBody;
        this.reasonForError = reasonForError;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataFixture() {
        return Arrays.asList(new Object[][]{
                {404, USER_NOT_FOUND_ERROR, BadAccountRegistrationRequestException.Reason.USER_MATCH_NOT_FOUND},
                {400, USER_ALREADY_REGISTERED_ERROR, BadAccountRegistrationRequestException.Reason.USER_ALREADY_EXISTS}
        });
    }

    @Test
    public void shouldReturnUserNotFoundExceptionFor404Status() {
        Response response = Response.builder().headers(new HashMap<>()).status(errorCode).body(errorBody.getBytes()).build();
        Exception exception = userClientErrorDecoder.decode("some method", response);

        assertEquals(new BadAccountRegistrationRequestException(reasonForError), exception);
    }
}