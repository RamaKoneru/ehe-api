package com.ehe.api.account.integration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.StringUtils;

public class FakeUserClient implements UserClient {

    @Override
    public User getUser(String firstName, String lastName, String gender, String email, String dateOfBirth) {
        if (StringUtils.equalsIgnoreCase(firstName, "Phil")
                && StringUtils.equalsIgnoreCase(lastName, "Filippelli")
                && StringUtils.equalsIgnoreCase(gender, "MALE")
                && StringUtils.equalsIgnoreCase(email, "Phil.Filippelli@ehe.health")
                && StringUtils.equalsIgnoreCase(dateOfBirth, LocalDate.of(1970, 1, 31).format(DateTimeFormatter.ISO_DATE))) {

            return new User(123L);
        }
        return null;
    }
}