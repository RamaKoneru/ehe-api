package com.ehe.api.account.service;

import com.ehe.api.account.integration.FakeUserClient;

public class AccountServiceObjectMother {

    public static AccountService create() {
        return new AccountService(new FakeUserClient());
    }
}