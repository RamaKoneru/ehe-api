package com.ehe.api.account.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import com.ehe.api.account.domain.Gender;
import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.dto.AccountDtoObjectMother;
import com.ehe.api.account.integration.FakeUserClient;

import org.junit.Test;

public class AccountServiceTest {

    private AccountDto accountDto = AccountDtoObjectMother.create();
    private AccountService accountService = new AccountService(new FakeUserClient());

    @Test
    public void successfulRegistrationShouldReturnAccountWithFirstName() {
        AccountDto account = accountService.registerAccount(accountDto);

        assertEquals("Phil", account.getFirstName());
    }

    @Test
    public void successfulRegistrationShouldReturnTheSameAccountObject() {
        AccountDto accountResult = accountService.registerAccount(accountDto);

        assertSame(accountDto, accountResult);
    }

    @Test
    public void successfulRegistrationShouldReturnGender() {
        AccountDto accountResult = accountService.registerAccount(accountDto);

        assertEquals(Gender.MALE.name(), accountResult.getGender());
    }
}