package com.ehe.api.account.controller;

import javax.validation.Valid;

import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/member/account")
@RestController
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<AccountDto> create(@Valid @RequestBody AccountDto accountRegistrationRequest) {
        AccountDto accountRegistrationResponse = accountService.registerAccount(accountRegistrationRequest);
        return new ResponseEntity<>(accountRegistrationResponse, HttpStatus.CREATED);
    }
}
