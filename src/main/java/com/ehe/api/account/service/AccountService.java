package com.ehe.api.account.service;

import com.ehe.api.account.dto.AccountDto;
import com.ehe.api.account.integration.User;
import com.ehe.api.account.integration.UserClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final UserClient userClient;

    @Autowired
    public AccountService(UserClient userClient) {
        this.userClient = userClient;
    }

    public AccountDto registerAccount(AccountDto accountDTO) {
        User user = userClient.getUser(accountDTO.getFirstName(), accountDTO.getLastName(), accountDTO.getGender(), accountDTO.getEmail(), accountDTO.getDateOfBirth());
        accountDTO.setUserId(user.getUserId());
        return accountDTO;
    }
}
