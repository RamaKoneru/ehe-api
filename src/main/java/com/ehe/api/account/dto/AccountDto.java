package com.ehe.api.account.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class AccountDto {
    @NotBlank(message = "Invalid first name.")
    private String firstName;
    @NotBlank(message = "Invalid last name.")
    private String lastName;
    @NotBlank(message = "Invalid gender.")
    private String gender;
    @Pattern(regexp = "\\d{4}-[0,1,2]{2}-[0,1,2,3]{2}", message = "Invalid date of birth.")
    private String dateOfBirth;
    @Email(message = "Invalid email.")
    private String email;
    private Long userId;
}
