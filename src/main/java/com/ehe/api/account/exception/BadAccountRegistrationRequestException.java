package com.ehe.api.account.exception;

import lombok.Data;

@Data
public class BadAccountRegistrationRequestException extends RuntimeException {

    private final Reason reason;

    public enum Reason {
        USER_MATCH_NOT_FOUND,
        USER_ALREADY_EXISTS,
        UNKNOWN
    }
}
