package com.ehe.api.account.domain;

public enum Gender {
    MALE,
    FEMALE,
    UNKNOWN
}
