package com.ehe.api.account.util;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperFactory {

    @Bean
    public ModelMapper create() {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper;
    }
}
