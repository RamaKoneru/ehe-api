package com.ehe.api.account.integration;

import java.io.IOException;

import com.ehe.api.account.dto.ErrorDetails;
import com.ehe.api.account.exception.BadAccountRegistrationRequestException;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.apache.commons.lang.StringUtils;

public class UserClientErrorDecoder implements ErrorDecoder {


    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 404) {
            return new BadAccountRegistrationRequestException(BadAccountRegistrationRequestException.Reason.USER_MATCH_NOT_FOUND);
        } else if (response.status() == 400) {
            return new BadAccountRegistrationRequestException(getReasonForBadDataError(response));
        }

        return null;
    }

    private BadAccountRegistrationRequestException.Reason getReasonForBadDataError(Response response) {
        try {
            ErrorDetails errorDetails = new ObjectMapper().readValue(response.body().asInputStream(), ErrorDetails.class);
            if (StringUtils.equalsIgnoreCase(errorDetails.getError(), "User already exists")) {
                return BadAccountRegistrationRequestException.Reason.USER_ALREADY_EXISTS;
            }

        } catch (IOException e) {
        }
        return BadAccountRegistrationRequestException.Reason.UNKNOWN;
    }
}
