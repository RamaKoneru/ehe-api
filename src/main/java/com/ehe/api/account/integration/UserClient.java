package com.ehe.api.account.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "epms-patient-service", url = "http://localhost:4545/users", configuration = UserClientConfiguration.class)
public interface UserClient {

    @RequestMapping(method = RequestMethod.GET, headers = "Content-Type=application/json")
    User getUser(@RequestParam("firstName") String firstName,
                 @RequestParam("lastName") String lastName,
                 @RequestParam("gender") String gender,
                 @RequestParam("email") String email,
                 @RequestParam("dateOfBirth") String dateOfBirth);
}
